(setq inhibit-startup-screen t)

(menu-bar-mode 0)
(tool-bar-mode 0)

(scroll-bar-mode t)

(setq visible-bell t)

;; (load-theme 'turing t)

(add-to-list 'custom-theme-load-path "~/.emacs.d/themes")
(load-theme 'dracula t)

(load "~/dev/lisp/timestamp/timestamp.el")

(set-default-font "DejaVu Sans Mono 11")
;; (set-default-font "a Attack Graffiti")

(global-linum-mode 0) ;;; Problems with term-mode-hook for not display numbers

(global-display-line-numbers-mode)
(setq display-line-numbers-type 'relative)
(setq-default show-trailing-whitespace t) ;; Resaltar trailing white spaces.


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(dolist (mode '(term-mode-hook
                shell-mode-hook
                eshell-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 0))))

(add-hook 'pdf-view-mode-hook (lambda () (global-linum-mode 0)))


;; Backup files
(setq backup-directory-alist `(("." . ,(expand-file-name "/tmp" user-emacs-directory)))) ;; *~


;; ORG presentation

(add-to-list 'load-path "~/.emacs.d/extras/org-present/")
(autoload 'org-present "org-present" nil t)

(eval-after-load "org-present"
  '(progn
     (add-hook 'org-present-mode-hook
               (lambda ()
                 (org-present-big)
                 (org-display-inline-images)
                 (org-present-hide-cursor)
                 (org-present-read-only)))
     (add-hook 'org-present-mode-quit-hook
               (lambda ()
                 (org-present-small)
                 (org-remove-inline-images)
                 (org-present-show-cursor)
                 (org-present-read-write)))))
;; https://github.com/rlister/org-present

;;;;;;;;;;;;;;;;;;;;;;
;; this is for export listings with latex-org-mode

;(add-to-list 'org-latex-packages-alist '("" "listings" nil))
;; (setq org-latex-listings nil)
(require 'org)

(setq org-src-preserve-indentation t
      org-src-fontify-natively t
      org-export-latex-listings t
      org-latex-listings 'listings
      org-latex-prefer-user-labels t
      org-confirm-babel-evaluate nil)
(add-to-list 'org-latex-packages-alist '("" "listings"))

(require 'ox-latex)

;; deactivate \hypersetup {}
(setq org-latex-with-hyperref nil)
;; https://stackoverflow.com/questions/11365739/how-to-cancel-the-hypersetup-in-0rg-mode-of-emacs


(defun my-org-latex-remove-title (str)
  (replace-regexp-in-string "^\\\\title{}$" "" str))

(advice-add 'org-latex-template :filter-return 'my-org-latex-remove-title)
;; https://stackoverflow.com/questions/57967064/disable-title-in-org-latex-export
;;;;;;;;;;;;;;;;;;;;;;
(setq-default message-log-max nil)
;; (kill-buffer "*Messages*")

(setq-default indent-tabs-mode nil)

(global-hl-line-mode 1)

(setq column-number-mode t)

(show-paren-mode 1)

(require 'iso-transl)

(defalias 'scroll-ahead 'scroll-up)

(defalias 'scroll-behind 'scroll-down)

(defun other-window-backward (&optional n)
  "Select Nth previous window."
  (interactive "P")
  (other-window (- (prefix-numeric-value n))))

(defun toggle-comment-on-line ()
  (interactive)
  (comment-or-uncomment-region (line-beginning-position) (line-end-position)))

(defun scroll-n-lines-ahead (&optional n)
  "Scroll ahead N lines"
  (interactive "P")
  (scroll-ahead (prefix-numeric-value n)))

(defun scroll-n-lines-behind (&optional n)
  "Scroll behind N lines"
  (interactive "P")
  (scroll-behind (prefix-numeric-value n)))

(defun read-only-if-symlink ()
  (if (file-symlink-p buffer-file-name)
      (progn
        (setq buffer-read-only t)
        (message "File is a symlink"))))

(defun ega/org-mode-visual-fill ()
  (setq visual-fill-column-width 100
        visual-fill-column-center-text t)
  (visual-fill-column-mode 1))

(defun ega/org-mode-setup ()
  (org-indent-mode)
  ;; (variable-pitch-mode 1)
  (auto-fill-mode 0)
  (visual-line-mode 1)
  (setq evil-auto-indent nil)
  ;; (diminish org-indent-mode) ;; This crashes with org-capture
  )

(defadvice switch-to-buffer (before existing-buffer
                                    activate compile)
  "When interactive, switch to existing buffers only,
  unless given prefix argument."
  (interactive
   (list (read-buffer "Switch to buffer:"
                      (other-buffer)
                      (null current-prefix-arg)))))

;; (global-set-key (kbd "C-c m c") 'mc/edit-lines)

(global-set-key (kbd "C-;") 'toggle-comment-on-line)

(global-set-key (kbd "C-x ,") 'comment-dwim)

(global-set-key (kbd "C-?") 'help-command)

(global-set-key (kbd "C-h") 'delete-backward-char)

(global-set-key (kbd "C-x C-o") 'other-window-backward)

(global-set-key (kbd "C-x C-¿") 'next-buffer)

(global-set-key (kbd "C-x C-'") 'previous-buffer)

(global-set-key (kbd "C-q") 'scroll-n-lines-behind)

(global-set-key (kbd "C-z") 'scroll-n-lines-ahead)

(global-set-key (kbd "C-x b") 'counsel-switch-buffer)

(global-set-key (kbd "C-x c") 'org-capture)

(global-set-key (kbd "C-x C-a") 'org-agenda)

(global-set-key (kbd "C-x C-u") 'mu4e)

(global-set-key (kbd "C-x m") 'mu4e-compose-new)

(global-set-key (kbd "C-x g") 'magit-status)

(require 'package)

(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("melpa-stable" . "https://stable.melpa.org/packages/")
                         ("org" . "https://orgmode.org/elpa/")
                         ("elpa" . "https://elpa.gnu.org/packages/")
                         ("marmalade" . "http://marmalade-repo.org/packages/")
                         ("tromey" . "http://tromey.com/elpa/")
                         ("gnu" . "https://elpa.gnu.org/packages/")))

(setq-default message-log-max nil)
(kill-buffer "*Messages*")

(setq-default indent-tabs-mode nil)

(global-hl-line-mode 1)

(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))
(unless (package-installed-p 'use-package)
  (package-install 'use-package))
(require 'use-package)
(setq use-package-always-ensure t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; MAIL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; in linux  the fonts goes on  ./fonts/ and update (When  add a new
;; one)  with  =fc-cache  -f  -v=.  With  fc-list  we  can  see  all
;; lists (Easy to find one with grep)

(use-package emojify
  :config
  (when (member "Segoe UI Emoji" (font-family-list))
    (set-fontset-font
     t 'symbol (font-spec :family "Segoe UI Emoji") nil 'prepend))
  (setq emojify-display-style 'unicode)
  (setq emojify-emoji-styles '(unicode))
  (bind-key* (kbd "C-c .") #'emojify-insert-emoji)) ; override binding in any mode

(use-package pdf-tools
  :ensure t
  :config
  (pdf-tools-install))

(use-package mu4e
  :ensure nil
  ;; :load-path "/usr/share/emacs/site-lisp/mu4e/"
  ;; :defer 20 ; Wait until 20 seconds after startup
  :config

  ;; This is set to 't' to avoid mail syncing issues when using mbsync
  (setq mu4e-change-filenames-when-moving t)

  ;; Refresh mail using isync every 10 minutes
  (setq mu4e-update-interval (* 10 60))
  (setq mu4e-get-mail-command "mbsync -a")
  (setq mu4e-maildir "~/Mail")

  ;; Configure the function to use for sending mail
  (setq message-send-mail-function 'smtpmail-send-it)

  ;; Make sure plain text mails flow correctly for recipients
  (setq mu4e-compose-format-flowed t)

  ;; Signature for the mails, for more lines add \n for multiple lines
  (setq mu4e-compose-signature sign-mail)

  (setq mu4e-contexts
        (list
         ;; Work account
         (make-mu4e-context
          :name "Ciencias"
          :match-func
            (lambda (msg)
              (when msg
                (string-prefix-p "/MailCiencias" (mu4e-message-field msg :maildir))))
          :vars '((user-mail-address . main-ciencias)
                  (user-full-name    . name)
                  (smtpmail-smtp-server  . "smtp.gmail.com")
                  (smtpmail-smtp-service . 465)
                  (smtpmail-stream-type  . ssl)
                  (mu4e-drafts-folder  . "/MailCiencias/[Gmail]/Drafts")
                  (mu4e-sent-folder  . "/MailCiencias/[Gmail]/Sent Mail")
                  (mu4e-refile-folder  . "/MailCiencias/[Gmail]/All Mail")
                  (mu4e-trash-folder  . "/MailCiencias/[Gmail]/Trash")))))

  (setq mu4e-maildir-shortcuts
      '(("/MailCiencias/Inbox"             . ?i)
        ("/MailCiencias/[Gmail]/Sent Mail" . ?s)
        ("/MailCiencias/[Gmail]/Trash"     . ?t)
        ("/MailCiencias/[Gmail]/Drafts"    . ?d)
        ("/MailCiencias/[Gmail]/All Mail"  . ?a))))

;; For enhace email with org-mode
(use-package org-mime
  :ensure t
  :config
  (setq org-mime-export-options '(:section-numbers nil
                                  :with-author nil
                                  :with-toc nil))
  (add-hook 'message-send-hook 'org-mime-htmlize))

(use-package htmlize
  :defer t
  :config
  (progn

    ;; It is required to disable `fci-mode' when `htmlize-buffer' is called;
    ;; otherwise the invisible fci characters show up as funky looking
    ;; visible characters in the source code blocks in the html file.
    ;; http://lists.gnu.org/archive/html/emacs-orgmode/2014-09/msg00777.html
    (with-eval-after-load 'fill-column-indicator
      (defvar modi/htmlize-initial-fci-state nil
        "Variable to store the state of `fci-mode' when `htmlize-buffer' is called.")

      (defun modi/htmlize-before-hook-fci-disable ()
        (setq modi/htmlize-initial-fci-state fci-mode)
        (when fci-mode
          (fci-mode -1)))

      (defun modi/htmlize-after-hook-fci-enable-maybe ()
        (when modi/htmlize-initial-fci-state
          (fci-mode 1)))

      (add-hook 'htmlize-before-hook #'modi/htmlize-before-hook-fci-disable)
      (add-hook 'htmlize-after-hook #'modi/htmlize-after-hook-fci-enable-maybe))

    ;; `flyspell-mode' also has to be disabled because depending on the
    ;; theme, the squiggly underlines can either show up in the html file
    ;; or cause elisp errors like:
    ;; (wrong-type-argument number-or-marker-p (nil . 100))
    (with-eval-after-load 'flyspell
      (defvar modi/htmlize-initial-flyspell-state nil
        "Variable to store the state of `flyspell-mode' when `htmlize-buffer' is called.")

      (defun modi/htmlize-before-hook-flyspell-disable ()
        (setq modi/htmlize-initial-flyspell-state flyspell-mode)
        (when flyspell-mode
          (flyspell-mode -1)))

      (defun modi/htmlize-after-hook-flyspell-enable-maybe ()
        (when modi/htmlize-initial-flyspell-state
          (flyspell-mode 1)))

      (add-hook 'htmlize-before-hook #'modi/htmlize-before-hook-flyspell-disable)
      (add-hook 'htmlize-after-hook #'modi/htmlize-after-hook-flyspell-enable-maybe))))

;;;;;;;;;;;;;;;;;

(add-to-list 'auto-mode-alist '("\\.\\(pl\\|pro\\|lgt\\)" . prolog-mode))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(use-package ivy
  :diminish
  :bind (("C-s" . swiper)
         :map ivy-minibuffer-map
         ("TAB" . ivy-alt-done)
         ("C-f" . ivy-alt-done)
         ("C-l" . ivy-alt-done)
         ("C-j" . ivy-next-line)
         ("C-k" . ivy-previous-line)
         :map ivy-switch-buffer-map
         ("C-k" . ivy-previous-line)
         ("C-l" . ivy-done)
         ("C-d" . ivy-switch-buffer-kill)
         :map ivy-reverse-i-search-map
         ("C-k" . ivy-previous-line)
         ("C-d" . ivy-reverse-i-search-kill))
  :config)
(ivy-mode 1)

(use-package ivy-rich
  :init
  (ivy-rich-mode 1))

(use-package counsel
  :after ivy
  :bind (("M-x" . counsel-M-x)
         ;; ("C-x C-f" . counsel-find-file)
         ;; ("C-M-l" . counsel-imenu)
         :map minibuffer-local-map
         ("C-r" . 'counsel-minibuffer-history))
  :custom
  (counsel-linux-app-format-function #'counsel-linux-app-format-function-name-only)
  :config
  (setq ivy-initial-inputs-alist nil)) ;; Don't start searches with ^

(use-package doom-modeline
  :ensure t
  :init (doom-modeline-mode 1))

(use-package minions
  :hook (doom-modeline-mode . minions-mode)
  :custom
  (minions-mode-line-lighter ""))

(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

(use-package which-key
  :init (which-key-mode)
  :diminish which-key-mode
  :config
  (setq which-key-idle-delay 0))

(use-package projectile
  :diminish projectile-mode
  :config (projectile-mode)
  :custom ((projectile-completion-system 'ivy))
  :demand t
  :bind-keymap
  ("C-c p" . projectile-command-map)
  :init
  (when (file-directory-p "~/dev") ;; Change to Ciencias? its the main directory for projects
    (setq projectile-project-search-path '("~/dev")))
  (setq projectile-switch-project-action #'projectile-dired))

(use-package counsel-projectile
  :config (counsel-projectile-mode))

(use-package magit
  :custom
  (magit-display-buffer-function #'magit-display-buffer-same-window-except-diff-v1))

;; All this is for magit-todos
;; https://github.com/alphapapa/magit-todos
(use-package async)
(use-package dash)
(use-package f)
(use-package hl-todo)
(use-package pcre2el)
(use-package s)
(use-package magit-todos)

(use-package nyan-mode)
(nyan-mode t)
(nyan-toggle-wavy-trail)
(nyan-start-animation)

(use-package fill-column-indicator)
(define-globalized-minor-mode global-fci-mode fci-mode (lambda () (fci-mode t)))
;; (global-fci-mode t) ;; activa fci-mode por siempre
;; (setq fci-rule-color "red") ;; color de la columna límite
;; (setq fci-rule-column 81) ;; establece el límite en 80

(require 'autopair)
(electric-pair-mode 1);; autocompleta: (), [], {}, "", ''
(setq electric-pair-pairs
      '(
        (?\" . ?\")
        (?\{ . ?\})
        (?\' . ?\')))
(require 'rainbow-delimiters)

(use-package haskell-mode)

;; for babel
;; https://github.com/victorolinasc/ob-elixir
(add-to-list 'load-path "~/.emacs.d/languages/elixir/ob-elixir")
;; for htmlize
;; https://github.com/hniksic/emacs-htmlize
(add-to-list 'load-path "~/.emacs.d/emacs-htmlize")
(use-package elixir-mode)

(use-package php-mode)

(use-package racket-mode)

(use-package rust-mode)

;;;;;;;;;;;;;;;;;

(defun go-mode-setup ()
  (setq tab-width 4))

(add-hook 'go-mode-hook 'go-mode-setup)

(defun kill-orgs ()
  (interactive)
  (setq kill--orgs '("Tasks.org"
                     "Ciencias.org"
                     "Journal.org"
                     "Birthdays.org"
                     "GoCal.org"))
  (loop for x in kill--orgs
        do
        (kill-buffer x)))

;;;;;;;;;;;;;;;;;

(setq org-src-tab-acts-natively t)

(org-babel-do-load-languages
 'org-babel-load-languages
 '((python . t)
   (matlab . t)
   (C . t)
   (haskell . t)
   (elixir . t)))

(use-package babel)

(use-package org
  :defer t
  :hook (org-mode . ega/org-mode-setup)
  :config
  (setq org-ellipsis " ▾"
        org-src-tab-acts-natively t
        org-edit-src-content-indentation 2
        org-hide-block-startup nil
        org-src-preserve-indentation nil
        org-cycle-separator-lines 2)

  (setq org-agenda-start-with-log-mode t)
  (setq org-log-done 'time)
  (setq org-log-into-drawer t)
  (setq org-agenda-files
        '("~/Documents/OrgFiles/Tasks.org"
          "~/Documents/OrgFiles/Ciencias.org"
          "~/Documents/OrgFiles/Journal.org"
          "~/Documents/OrgFiles/Birthdays.org"
          "~/Documents/OrgFiles/GoCal.org"))

  (setq org-refile-targets
        '(("Log.org" :maxlevel . 1)
          ("Tasks.org" :maxlevel . 1)))

  (advice-add 'org-refile :after 'org-save-all-org-buffers)

  (setq org-tag-alist
        '((:startgroup)
                                        ; Put mutually exclusive tags here
          (:endgroup)
          ("email" . ?e)
          ("extra" . ?E)
          ("home" . ?h)
          ("idea" . ?i)
          ("meetings" . ?m)
          ("notes" . ?n)
          ("planning" . ?p)
          ("programming" . ?P)
          ("quote" . ?q)
          ("school" . ?s)
          ("work" . ?w)
          ;; ciencias ayudantias
          ("end" . ?f)
          ("done" . ?d)
          ("maybe" . ?y)))

  (setq org-todo-keywords
        '((sequence "TODO(t)" "DOING(n)" "|" "DONE(d!)")
          ;; (sequence "...") can add other sequence here
          ))

  (setq org-capture-templates
        `(("t" "Tasks / Projects")
          ("tt" "Task" entry (file+olp "~/Documents/OrgFiles/Tasks.org" "Inbox")
           "* TODO %?\n  %U\n  %a\n  %i" :empty-lines 1)

          ("j" "Journal Entries")
          ("jj" "Journal" entry
           (file+olp+datetree "~/Documents/OrgFiles/Journal.org")
           "\n* %<%I:%M %p> - Journal :journal:\n\n%?\n\n"
           ;; ,(dw/read-file-as-string "~/Notes/Templates/Daily.org")
           :clock-in :clock-resume
           :empty-lines 1)
          ("jm" "Meeting" entry
           (file+olp+datetree "~/Documents/OrgFiles/Journal.org")
           "* %<%I:%M %p> - %a :meetings:\n\n%?\n\n"
           :clock-in :clock-resume
           :empty-lines 1)
          ("g" "Google Calendar")
          ("gg" "GoCal" entry (file+olp "~/Documents/OrgFiles/GoCal.org" "Google Calendar")
           "* TODO %?\n %U\n %a\n %i" :empty-lines 1)
          ))

  ;; EXAMPLE OF CAPTURE TABLE
  ;; ("m" "Metrics Capture")
  ;; ("mw" "Weight" table-line (file+headline "~/Documents/OrgFilesMetrics.org" "Weight")
  ;; "| %U | %^{Weight} | %^{Notes} |" :kill-buffer t)

  )

(use-package org-bullets
  :after org
  :hook (org-mode . org-bullets-mode)
  :custom
  (org-bullets-bullet-list '("◉" "○" "●" "○" "●" "○" "●")))

(use-package visual-fill-column
  :hook (org-mode . ega/org-mode-visual-fill))

;;;;;;;;;;;;;;;;;;;;

                                        ; * TODO: eshell as a good shell, need more info and customs

(use-package ein)

(use-package yasnippet
  :hook
  (prog-mode . yas-minor-mode)
  (latex-mode . yas-minor-mode) ;; This is bc latex-mode is not in prog-mode
  :config
  (yas-reload-all))

;; (yas-minor-mode 1)

;; (use-package iedit)

;;;;;;;;;;;;;;;;;;;;
;; LSP (new)

(use-package markdown-mode)

;; lsp-package
(use-package lsp-mode
  :commands (lsp lsp-deferred)
  :init
  (setq lsp-keymap-prefix "C-c l")  ;; Or 'C-l', 's-l'
  :config
  (lsp-enable-which-key-integration t))

(use-package lsp-treemacs
  :after lsp)

(use-package lsp-ivy)

;; java mode
;; TODO fix
(use-package lsp-java)
(add-hook 'java-mode-hook #'lsp)
;; java workspace: Maybe set workspace for each laguage (?)
(setq lsp-java-workspace-dir "~/Ciencias/APIs/")

;;; language: typescript
(use-package typescript-mode
  :mode "\\.ts\\'"
  :hook (typescript-mode . lsp-deferred)
  :config
  (setq typescript-indent-level 2))

;; dashboard
(use-package dashboard
  :ensure t
  :config
  (dashboard-setup-startup-hook))
(setq recentf-exclude '("~/Documents/OrgFiles/*.org"))
;; Set the dashboard title
(setq dashboard-banner-logo-title "I'm not a great programmer; I'm just a good programmer with great habits.")

(use-package command-log-mode)

(use-package perspective
  :ensure t
  :bind (("C-x k" . persp-kill-buffer*)
         ("C-x b" . persp-counsel-switch-buffer))
  :custom
  (persp-initial-frame-name "Main")
  :init
  (persp-mode))

;;;;;;;;;;;;;; GAMES
(use-package 2048-game)


(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(ansi-color-names-vector
   ["black" "red3" "ForestGreen" "yellow3" "blue" "magenta3" "DeepSkyBlue" "gray50"])
 '(counsel-linux-app-format-function (function counsel-linux-app-format-function-name-only) nil nil "Customized with use-package counsel")
 '(custom-enabled-themes (quote (dracula)))
 '(custom-safe-themes
   (quote
    ("707c40cd994c8d19a89b326978b14d7ba82881f56814fbd67b4ab72b49040edd" default)))
 '(magit-display-buffer-function (function magit-display-buffer-same-window-except-diff-v1) nil nil "Customized with use-package magit")
 '(magit-todos-insert-after (quote (bottom)) nil nil "Changed by setter of obsolete option `magit-todos-insert-at'")
 '(minions-mode-line-lighter "" nil nil "Customized with use-package minions")
 '(org-bullets-bullet-list (quote ("◉" "○" "●" "○" "●" "○" "●")) nil nil "Customized with use-package org-bullets")
 '(org-export-backends (quote (ascii html icalendar latex md odt)))
 '(package-selected-packages
   (quote
    (babel htmlize rust-mode emojify magit-todos pcre2el hl-todo 2048-game osc org-mime elixir-mode php-mode perspective command-log-mode dashboard lsp-ivy lsp-java typescript-mode markdown-mode lsp-mode iedit pdf-tools go-mode competitive-programming-snippets yasnippet ein eshell-git-prompt vterm which-key visual-fill-column use-package rainbow-delimiters racket-mode org-bullets nyan-mode minions magit ivy-rich haskell-mode fill-column-indicator doom-modeline counsel-projectile autopair)))
 '(persp-initial-frame-name "Main" nil nil "Customized with use-package perspective")
 '(projectile-completion-system (quote ivy) nil nil "Customized with use-package projectile")
 '(send-mail-function (quote smtpmail-send-it))
 '(smtpmail-smtp-server "smtp.gmail.com")
 '(smtpmail-smtp-service 587)
 '(timestamp-ext ".txt"))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
